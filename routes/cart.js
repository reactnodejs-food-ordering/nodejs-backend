const {Cart} = require('../models/cart');
const express = require('express');
const router =  express.Router();
const Joi = require('joi');

//To prevent errors from Cross Origin Resource Sharing, we will set 
//our headers to allow CORS with middleware like so:
router.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.setHeader("Access-Control-Allow-Headers", 
     "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
   //and remove cacheing so we get the most recent comments
    res.setHeader("Cache-Control", "no-cache");
    next();
});


router.get("/:id", async(req, res) => {

    console.log("getting cart info ... ", req.params.id);

    const foodCart = await Cart.findById(req.params.id);
    if(!foodCart) return res.status(404).send("Cart with given ID cannot be found");
    console.log("existing cart >> ", foodCart);
    res.send(foodCart);
});



router.post('/', async(req, res) => {
    console.log("creating new food cart ... ");

    let foodCart = new Cart({
        'createdDate': new Date(),
        // 'orderDate': new Date(),
        // 'items': [ {
        //     'menu_id': req.body._id,
        //     'quantity': +1
        // }]
    });

    const newCart = await foodCart.save();
    console.log("created new cart > ", newCart)
    res.send(newCart);
});

router.post("/:cartId/item/:menuId", async(req, res) => {

    console.log('cart id ', req.params.cartId);
    console.log('menu id ', req.params.menuId);
    console.log('menu body > ', req.body);

    const foodCart = await Cart.findById(req.params.cartId);
    if(!foodCart) return res.status(404).send("Cart with given ID cannot be found");

    console.log('aaa ', foodCart.items);

    // const foundItem = foodCart.items.filter(item => item.menu_id === req.body._id);
    // if(foundItem.length > 0) {
    //     foundItem[0].quantity += 1;
    //     foodCart.items
    // }
    // else {
    //     let item = {
    //         'menu_id': req.body._id,
    //         'name': req.body.name,
    //         'quantity': 1
    //     };
    //     foodCart.items.push(item);
    // }

    foodCart.items = req.body;

    const newcart = await foodCart.save();
    console.log('after update > ' , newcart);
});


// router.post('/', async(req, res) => {
//     console.log("creating new food cart ... ");
//
//     let foodCart = new Cart({
//         'createdDate': new Date(),
//         'orderDate': new Date(),
//         'items': [ {
//             'menu_id': req.body._id,
//             'quantity': +1
//         }]
//     });
//
//     const newCart = await foodCart.save();
//     console.log("created new cart > ", newCart)
//     res.send(newCart);
// });


router.post("/:id", async(req, res) => {
    console.log("updating food cart ...");

    const cart = await Cart.findById(req.params.id);
    if(!cart) return res.status(404).send("Cart with given ID cannot be found");

    cart.items = req.body;
    const updatedCart = await cart.save();
    res.status(200).send(updatedCart);
});

module.exports = router;

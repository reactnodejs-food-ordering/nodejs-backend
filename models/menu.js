const mongoose = require('mongoose');
const Joi = require('joi');



//compile schema into model
const Menu = mongoose.model('Menu', new mongoose.Schema({
    name: {
        type: String,
        required: true,
        maxlength: 255
    },
    price: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true,
        // enum: ['Western', 'Chinese', 'Japanese']
    },
    imageUrl: {
        type: String,
        required: true,
        maxlength: 255
    }
}));


function validateMenu(menu) {
    const schema = {
      name: Joi.string().min(5).max(50).required(),
      price: Joi.string().min(5).max(50).required(),
      category: Joi.string().min(5).max(50).required(),
      imageUrl: Joi.string().required()
    };
  
    return Joi.validate(menu, schema);
  }


  exports.Menu = Menu;
  exports.validate = validateMenu;
const mongoose = require('mongoose');
const Joi = require('joi');


//compile schema into model
const Cart = mongoose.model('Cart', new mongoose.Schema({
    createdDate: {
      type: Date,
      default: Date.now
    },
    items: {
        type: Array,
        object: {
            _id: {
                type: String,
                required: true
            },
            name: {
                type: String,
                required: true
            },
            price: {
                type: String,
                required: true
            },
            imageUrl: {
                type: String,
                required: true
            },
            quantity: {
                type: Number,
                require: true,
                default: 0
            }
        } 
    }
}));


exports.Cart = Cart;
